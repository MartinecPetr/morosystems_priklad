import React, { Component } from 'react';
import PropTypes from 'prop-types';

class DeleteBtn extends Component {
	render() {
		return (
		  <td>
		    <button type="button" class="btn btn-danger" onClick={this.props.onRowDeleted.bind(this)}>Delete</button>
		  </td>
		);
	}
}

DeleteBtn.propTypes = {
  onRowDeleted: PropTypes.function,
};

export default DeleteBtn;