import React, { Component } from 'react';
import Base64 from 'base-64';
import Table from './Table';
import AddRow from './AddRow';
import superagent from 'superagent';

class Users extends Component {


  constructor(props) {
    super(props);
    this.users = [];
  };

  componentDidMount(){
    this.getAllUsersApi();
  }

  getAllUsersApi(){
      superagent
      .get("/users/all")
      .set('Accept', 'application/json')
      .end ((error, response)=>{
          this.users = JSON.parse(response.text);     
          this.setState(this.users);      
      });
  }

  createUserApi(userName){
      superagent
      .post("/users/new")
      .send({ name: userName })
      .set('Accept', 'application/json')
      .end ((error, response)=>{
        if(error == null){
          this.users.push(JSON.parse(response.text));
          this.setState(this.users);
        }
      });
  }

  updateUserApi(user){
      superagent
      .put("/users/edit")
      .send(JSON.stringify(user))
      .set('Content-Type', 'application/json')
      .end ((error, response)=>{
        if(error == null){
          var editUser = JSON.parse(response.text);

          var index = this.users.map(function(e) { return e.id; }).indexOf(editUser.id);
          this.users[index].name = editUser.name;
          this.setState(this.users);
        }
      });
  }

  deleteUserApi(user){
      superagent
      .delete("/secured/deleteUser/" + user.id)
      .set('Content-Type', 'application/json')
      .set('authorization', "Basic " + Base64.encode(user.name + ":password"))
      .end ((error, response)=>{
        if(error == null){
          var index = this.users.map(function(e) { return e.id; }).indexOf(user.id);
          this.users.splice(index, 1);
          this.setState(this.users);
        }
      });
  }

  handleRowDel(user) {        
    this.deleteUserApi(user);
  };

  handleRowAdd(userName) {
    this.createUserApi(userName);
  };

  handleRowEdit(user) {
    this.updateUserApi(user);
  };

  render() {
    return (
      <div>
        <Table cols={{id: "ID", name: "Name",}} rows={this.users} onRowDeleted={this.handleRowDel.bind(this)} onRowEdit={this.handleRowEdit.bind(this)}/>
        <AddRow onRowAdded={this.handleRowAdd.bind(this)}/>
      </div>
    );
  }
}

export default Users;