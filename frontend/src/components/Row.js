import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Cell from './Cell';
import EditBtn from './EditBtn';
import DeleteBtn from './DeleteBtn';

class Row extends Component {
  constructor(props) {
    super(props);
    this.editEnable = false;
  };
  onDelEvent() {
    this.props.onRowDeleted(this.props.row);
  }
  render() {
    return (
      <tr>
        <Cell value={this.props.row.id}/>
        <Cell value={this.props.row.name}/>
        <EditBtn onRowEdit={this.props.onRowEdit.bind(this)} data={this.props.row}/>
        <DeleteBtn onRowDeleted={this.onDelEvent.bind(this)}/>
      </tr>
    );
  }
}

Row.propTypes = {
  row: {
    id: PropTypes.string,
    name: PropTypes.string,
  },
  onRowDeleted: PropTypes.function,
  onRowEdit: PropTypes.function,
};
export default Row;