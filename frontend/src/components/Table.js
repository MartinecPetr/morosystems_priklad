import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Head from './Head';
import Body from './Body';

class Table extends Component {
  render() {
    return (
      <table class="table">
        <Head col={this.props.cols}/>
        <Body rows={this.props.rows} onRowDeleted={this.props.onRowDeleted.bind(this)} onRowEdit={this.props.onRowEdit.bind(this)}/>
      </table> 
    );
  }
}

Table.propTypes = {
  cols: PropTypes.object,
  rows: PropTypes.array,
  onRowDeleted: PropTypes.function,
};

export default Table;