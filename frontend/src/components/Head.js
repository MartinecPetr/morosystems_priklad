import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Head extends Component {
  render() {
    return (
      <tr>
          <th>{this.props.col.id}</th>
          <th>{this.props.col.name}</th>
          <th></th>
          <th></th>
        </tr>
    );
  }
}
Head.propTypes = {
  col: {
    id: PropTypes.string,
    name: PropTypes.string,
  },
};
export default Head;