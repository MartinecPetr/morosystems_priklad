import React, { Component } from 'react';
import PropTypes from 'prop-types';

class AddRow extends Component {

  constructor(props) {
    super(props);
    this.name = "";
  }
  onRowAdd() {
  	this.props.onRowAdded(this.name);
  	this.name = "";
  }
  updateInputValue(evt) {
      this.name = evt.target.value;
  }
  render() {
    return (  	
          <div class="form-group">
		      <div class="form-check">
			    <label for="exampleInputEmail1" value={this.props.name}>Name</label>
			    <input type="text" class="form-control" onChange={this.updateInputValue.bind(this)}/>
				<button type="submit" class="btn btn-primary" onClick={this.onRowAdd.bind(this)}>Save</button>
		      </div>
	      </div>
    );
  }
}

AddRow.propTypes = {
  onRowAdded: PropTypes.function,
  name: PropTypes.string,
};

export default AddRow;
