import React, { Component } from 'react';
import PropTypes from 'prop-types';

class EditBtn extends Component {
	
	constructor(props) {
		super(props);
		this.inputText = this.props.data.name;
	};

	updateInputValue(evt) {
			this.inputText = evt.target.value;
			this.forceUpdate()
	}

	saveEdit(){
		var newUser = {id: this.props.data.id, name: this.inputText}
		this.props.onRowEdit(newUser);
	}

	render() {
		return (        
			<td>
				<input type="text" value={this.inputText} onChange={this.updateInputValue.bind(this)}/>
				<button type="button" class="btn btn-warning" onClick={this.saveEdit.bind(this)}>Edit</button>
			</td>
		);
	}
}

EditBtn.propTypes = {
	data: {
		id: PropTypes.number,
		name: PropTypes.string,
	},
};

export default EditBtn;
