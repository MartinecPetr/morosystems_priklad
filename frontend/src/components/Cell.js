import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Cell extends Component {
  render() {
    return (
      <td>{this.props.value}</td>
    );
  }
}

Cell.propTypes = {
  value: PropTypes.oneOfType([
	  PropTypes.string,
	  PropTypes.number
	]),
};

export default Cell;