import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Row from './Row';

class Body extends Component {
  render() {
    var onDelete = this.props.onRowDeleted.bind(this);
    var onEdit = this.props.onRowEdit.bind(this);
    var data = this.props.rows.map(function(row) {
      return (<Row row={{
                id: row.id,
                name: row.name,
              }}
              onRowDeleted={onDelete}
              onRowEdit={onEdit}
              />);
    });
    return (
      data
    );
  }
}

Body.propTypes = {
  rows: PropTypes.array,
  onRowDeleted: PropTypes.function,
};
export default Body;