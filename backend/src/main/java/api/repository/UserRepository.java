package api.repository;

import api.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {
    User getUserById(int id);
    User findByUsername(String username);
}
