package api.controller;

import api.entity.User;
import api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@RestController

@RequestMapping("/secured")
public class SecuredUserController {

    @Autowired
    private UserService userService;

    @DeleteMapping("/deleteUser/{id}")
    public void getAllUsers(@PathVariable("id") int id, HttpServletResponse response){
        userService.deleteUserById(id);
    }
}
