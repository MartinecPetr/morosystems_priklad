package api.controller;


import api.entity.User;
import api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/all")
    public Iterable<User> getAllUsers(){
        return userService.getAllUsers();
    }

    @GetMapping("/{id}")
    public User getAllUsers(@PathVariable("id") int id, HttpServletResponse response){
        User user = userService.getUserById(id);
        if( null == user ){
            response.setStatus(HttpStatus.NO_CONTENT.value());
        }
        return user;
    }

    @PutMapping("/edit")
    public User editUser(@RequestBody User u, HttpServletResponse response){
        User user = userService.editUser(u);
        if( null == user ){
            response.setStatus(HttpStatus.NO_CONTENT.value());
        }
        return user;
    }

    @PostMapping("/new")
    public User createUser(@RequestBody User user){
        if(user.getUsername() == null)
            user.setUsername(user.getName());
        return userService.createUser(user);
    }
}
